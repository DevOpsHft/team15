# DevOpsWorkshop

This repository is just a starting point to have something to play with. People in the workshop will clone it, and then change at will - version2.

See the [GitHub Gist](https://gist.github.com/mliebelt/31775ea09e1b3a786df2f01f279d54b8) about the preparation of that.

## Initial Setup for Each Team

1. Start by using the base URL for your project, e.g. `https://gitlab.com/DevOpsHft/team<NR>.git` You have to replace the `<NR>` part of it.
1. Do a `git clone <base URL>` in a new directory.
1. Have a look in the plan directories for the steps to do.

(There is additional information told to you during the lecture, and only there, how to push commits later.)

## Plans

So we will follow a planed storyboard, so that everyone can go at its own pace. Here you will find the "plans" that are prepared.

1. Start with a core build process, with nearly no functionality.
1. Introduce the build, with errors.
1. Complete the build the first time, with a docker image as result.
1. Push the image to the docker registry.
1. Deploy the image somehow.
1. Deploy to a team namespace to avoid collisions.
1. Scale the deployment.

Each of those plans has its own directory, which is located under plans. You have to copy the files, or create them on your own, if you want.

Please read each time carefully the `README.md` file located there. This includes necessary information about the plan to follow.

## Base steps in all exercises

* `cd <DIR>` (depends on the cloned repository)
* `more plan<NR>/README.md` (or look it up in the UI)
* Do some changes in the directory itself, or in `src`.
* `git add -A` (only necessary for completely new files)
* `git commit -a -m"<your message>"` (commit all changes)
* `git push` (Push the changes to the central repository)
* Wait for the build server to finish its job. The build server in our exercises is Gitlab-CI, but should work similar with Jenkins, TravisCI, Circle-CI, ...

## Cheat Sheets

To make the exercises on your own, you have to have a basic understanding how Docker and Kubernetes work. Here is a list of cheatsheets for details. **You don't have to use those, everything is included in the Base Steps below.**

* Git
  * [Git Cheatsheet](https://www.git-tower.com/blog/git-cheat-sheet) to have the base commands available
* Docker
  * [Docker Cheatsheet](https://github.com/wsargent/docker-cheat-sheet) Very detailed, ...
  * [Docker Cheatsheet (small)](https://devhints.io/docker) Very dense, perfect for looking up commands
* Kubernetes
  * [Kubernetes Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) More a reference (everything you could possibly know ...)
  * [Kubernetes Commands](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands) Detailed documentation to all commands of `kubectl`.
  
## Public `Playgrounds`

* [Docker Playground](https://labs.play-with-docker.com/) Login with Docker
* [Kubernetes (K8s) Playground](https://labs.play-with-k8s.com) Login with Docker or Github

If you want to get your feets wet, and try it out in a native environment.
