# Deployment

To have a deployment, the `kubectl` configuration has to be described, here in the file `deployment.yaml`. And the build has to be adapted, so that automatic deployments are done.

Changing something means, that the whole build process is running then, and at the end, the deployment just changes what need to be changed:

* Images
* Scaling
* Sizing
* ...

## Steps

* Copy `deployment.yml` to the root directory. There are some changes needed, without them, you will later not find your deployed service!!
  * Adapt the name of the service. This is referenced several times!!
  * **Ensure to change the project from `teamref` to your team label.**
* Copy the changed `.gitlab-ci.yml` file as well.

## Result

* You see one stage more in the build process.
* As a final step, the deployment through `kubectl` will be triggered.
* Please ask the workshop owner to help expose your running application, so you can see it on the internet. From that point, your application is **live**!!

## Cloud Creation (only for workshop owner)

* Create a project in Google Cloud (this will take some time)
* Enable the Kubernetes API (this will take some time)
* (locally) Ensure you have installed the Google Cloud SDK (resulting in binary gcloud)
* Call `gcloud auth login` which redirects you to the login of Google.
* Call `gcloud config set project <project-ID>`
* Create a cluster with the Google Cloud web UI.
* Create a service account to use that in the build job.

## References

* https://stackoverflow.com/questions/47888027/how-to-deploy-staging-in-google-cloud-platform-with-kubernetes-and-gitlab-ci-cd Gives  the basic steps to have that enabled in .gitlab-ci.yml. Last step did not work for me
* https://stackoverflow.com/questions/53420870/keep-getting-permissions-error-gcloud-container-clusters-get-credentials