# Adding Namespace

## Steps

At the moment, you deploy your pod to the default namespace, used by everyone.
To isolate deployments, it is good to separate resources by namespaces.

Do the following:

1. Copy the file `namespace.yaml` to the root directory.
2. Ensure that you update the file according to your team context.
3. Ensure that the namespace is applied. Add the following line to your `.gitlab-ci` file. This has to go before the deployment is done.
   `kubectl apply -f namespace.yaml`
4. Add to the file `deployment.yaml` the following section:
```YAML
...
metadata:
  name: simpleweb
  namespace: teamXX
...
```
   Of course with the right namespace name.

You may change anything else about your application.

Get in touch with the workshop leader how the result can be seen live ... He is the only one that is allowed to do that.

## Result

You should expect the following:

* After having pushed your changes, the build is done automatically.
* After some time, you should see all stages succeed.
* After the last stage, your deployment will be triggered, so the new namespace and your deployed application is visible there.
* What about the previously deployed web site?